import React, {Component} from 'react';
import AnimalSelect from './AnimalSelect';
import Cage from './Cage';


export default class Exhibit extends Component {

  constructor(props) {
    super(props);
    this.state =  {
        selectedAnimal : this.props.selectedAnimal
    };
    this.setAnimal = this.setAnimal.bind(this)
  }
  setAnimal(newAnimal) {
      this.setState({ selectedAnimal : newAnimal});
  }

  render () {
      const {selectedAnimal} = this.state;
  	return (
	    <div className="exhibit">
            <Cage selectedAnimal={selectedAnimal} />
            <AnimalSelect submitAnimal={this.setAnimal} animals={this.props.animals} />
	    </div>
  		)
  }
};

