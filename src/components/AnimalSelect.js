import React, { Component } from 'react';

// exportando la función constructora (componente tonto)
// cual es el parametro entrando aca?

export default class AnimalSelect extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            animals: this.props.animals,
        };
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event)
    {
        const animal = event.target.value;
        this.props.submitAnimal(animal);
    }


    render(){
        const animalsResult = this.state.animals.map(animal => {
            return <option key={animal} value={animal}>{animal}</option>;
        });
        return (
            <form>
                <label>Select an Animal: </label>
                <select onChange={this.handleChange}>
                    {animalsResult}
                </select>
            </form>
        )

    }


};

