import {SET_MAMMAL,SET_BIRD,SET_FISH} from '../../constants/main';

export const setMammal = animal => ({
    type: SET_MAMMAL,
    animal: animal
});

export const setBird = animal => ({
    type: SET_BIRD,
    animal: animal,
});
export const setFish = animal => ({
    type: SET_FISH,
    animal: animal,
});
