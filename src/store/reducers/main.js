import update from 'immutability-helper';
import {SET_MAMMAL,SET_BIRD,SET_FISH} from '../../constants/main';

const mammals = ['Tiger', 'Panda', 'Pig'];
const birds = ['Eagle', 'Flamingo', 'Penguin'];
const fish = [ 'Seahorse', 'Octopus', 'Stingray'];

const initialState = {
    selectedMammal : "Tiger",
    selectedBird : "Eagle",
    selectedFish : "Seahorse",
    mammals,
    birds,
    fish
};
	// Asegurate que entendes los parametros aquí!
	// con cualquier reducer esperamos 2 argumentos
	// somos capaces de dar un valor por defecto a el parametro en la forma vista abajo
export default (state = initialState, { type, ...action} = {}) => {
    switch (type) {
        case SET_MAMMAL: {
            return update(state, {
                selectedMammal: { $set: action.animal },
            });
        }
        case SET_BIRD: {
            return update(state, {
                selectedBird: { $set: action.animal },
            });
        }
        case SET_FISH: {
            return update(state, {
                selectedFish: { $set: action.animal },
            });
        }
        default:
            return state;
    }
};
